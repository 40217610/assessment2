﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for Edit_meal.xaml
    /// </summary>
    public partial class Edit_meal : Window
    {
        public static int b_num = EditBooking.r_num;

        public Edit_meal()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            if (cb3.IsChecked == true)
                Booking_factory.booking_register[b_num].BreakfastP = true;
            else 
                Booking_factory.booking_register[b_num].BreakfastP = false;

            if (cb4.IsChecked == true)
                Booking_factory.booking_register[b_num].DinnerP = true;
            else
                Booking_factory.booking_register[b_num].DinnerP = false;
            Booking_factory.booking_register[b_num].Dietery_reqP = txt1.Text;
            Close();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (Booking_factory.booking_register[b_num].BreakfastP == true)
                cb3.IsChecked = true;

            if (Booking_factory.booking_register[b_num].DinnerP == true)
                cb4.IsChecked = true;

            txt1.Text = Booking_factory.booking_register[b_num].Dietery_reqP;
             
        }
    }
}
