﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static bool opened_cus = false;
        public static bool opened_book = false;
        public Guest gs1, gs2, gs3, gs4;

        public MainWindow()
        {
            InitializeComponent();

            if (opened_cus == false)
            {
                Open_Cus();
                opened_cus = true;
            }

            if (opened_book == false)
            {
                Open_Book();
                opened_book = true;
            }
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btn1_Click_1(object sender, RoutedEventArgs e)
        {
            Customers newWin = new Customers();
            newWin.Show();
            Close();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            Bookings newWin = new Bookings();
            newWin.Show();
            Close();
        }

        private void btn3_Click_1(object sender, RoutedEventArgs e)
        {
            
        }

        private void btn3_Click_2(object sender, RoutedEventArgs e)
        {
            Save_Book();
            Save_Cus();
            Close();
        }

        private void Save_Cus()
        {
            Dictionary<int, Customer>.ValueCollection values = Customer_factory.customer_register.Values;

            foreach (Customer c in values)
            {
                using (StreamWriter writetext = new StreamWriter("write.txt"))
                {
                    writetext.WriteLine(c.NameP);
                    writetext.WriteLine(c.AddressP);
                }
                
            }
        }

        private void Save_Book()
        {
            Dictionary<int, Booking>.ValueCollection values = Booking_factory.booking_register.Values;

            
                using (StreamWriter writetext = new StreamWriter("write_b.txt"))
                {
                    foreach (Booking b in values)
                    {
                            writetext.WriteLine(b.Booking_refP);
                            writetext.WriteLine(b.A_dateP);
                            writetext.WriteLine(b.D_dateP);
                            writetext.WriteLine(b.Driver_nameP);
                            writetext.WriteLine(b.Dietery_reqP);
                            writetext.WriteLine(b.Car_a_dateP);
                            writetext.WriteLine(b.Car_d_dateP);
                            writetext.WriteLine(b.CustomerP);

                            if (b.BreakfastP == true)
                                writetext.WriteLine("true");
                            else
                                writetext.WriteLine("false");
                            if (b.DinnerP == true)
                                writetext.WriteLine("true");
                            else
                                writetext.WriteLine("false");
                            if (b.CarP == true)
                                writetext.WriteLine("true");
                            else
                                writetext.WriteLine("false");
                            
                            if (b.a == null)
                            {
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                            }
                            else
                            {
                                writetext.WriteLine(b.a.NameP);
                                writetext.WriteLine(b.a.AgeP.ToString());
                                writetext.WriteLine(b.a.PassportP);
                            }

                            if (b.b == null)
                            {
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                            }
                            else
                            {
                                writetext.WriteLine(b.b.NameP);
                                writetext.WriteLine(b.b.AgeP.ToString());
                                writetext.WriteLine(b.b.PassportP);
                            }
                            if (b.c == null)
                            {
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                            }
                            else
                            {
                                writetext.WriteLine(b.c.NameP);
                                writetext.WriteLine(b.c.AgeP.ToString());
                                writetext.WriteLine(b.c.PassportP);
                            }
                            if (b.d == null)
                            {
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                                writetext.WriteLine("empty");
                            }
                            else
                            {
                                writetext.WriteLine(b.d.NameP);
                                writetext.WriteLine(b.d.AgeP.ToString());
                                writetext.WriteLine(b.d.PassportP);
                            }
                           
                    }
                }

        }

        public void Open_Book()
        {
            String x0 = " ";
            String x1 = " ";
            String x2 = " ";
            String x3 = " ";
            String x4 = " ";
            String x5 = " ";
            String x6 = " ";
            String x7 = " ";
            String x8 = " ";
            String x9 = " ";

            String b8 = " ";
            String b9 = " ";
            String b10 = " ";

            bool bo8 = false;
            bool bo9 = false;
            bool bo10 = false;

            String g1;
            String g2_a;
            int g2 = 0;
            String g3;
            

            using (StreamReader readtext = new StreamReader("write_b.txt"))
            {
                while ((x1 != null) || (x2 != null))
                {
                    x0 = readtext.ReadLine();
                    x1 = readtext.ReadLine();
                    x2 = readtext.ReadLine();
                    x3 = readtext.ReadLine();
                    x4 = readtext.ReadLine();
                    x5 = readtext.ReadLine();
                    x6 = readtext.ReadLine();
                    x7 = readtext.ReadLine();
                    x8 = readtext.ReadLine();


                    b8 = readtext.ReadLine();
                    b9 = readtext.ReadLine();
                    b10 = readtext.ReadLine();

                    // Guest 1.
                    g1 = readtext.ReadLine();
                    g2_a = readtext.ReadLine();
                    g3 = readtext.ReadLine();
                    if (g1 == "empty")
                    {
                         gs1 = null;
                    }
                    else
                    {
                        gs1 = new Guest(g1, g2, g3);
                    }

                    // Guest 2.
                    g1 = readtext.ReadLine();
                    g2_a = readtext.ReadLine();
                    g3 = readtext.ReadLine();
                    if (g1 == "empty")
                    {
                         gs2 = null;
                    }
                    else
                    {
                        gs2 = new Guest(g1, g2, g3);
                    }
                       
                    // Guest 3.
                    g1 = readtext.ReadLine();
                    g2_a = readtext.ReadLine();
                    g3 = readtext.ReadLine();
                    if (g1 == "empty")
                    {
                         gs3 = null;
                    }
                    else
                    {
                        gs3 = new Guest(g1, g2, g3);
                    }

                    // Guest 4.
                    g1 = readtext.ReadLine();
                    g2_a = readtext.ReadLine();
                    g3 = readtext.ReadLine();
                    if (g1 == "empty")
                    {
                         gs4 = null;
                    }
                    else
                    {
                        gs4 = new Guest(g1, g2, g3);
                    }

                    if (x1 == "" || x1 == " " || x1 == null)
                        break;
                    else
                    {
                        Booking_factory.Create_booking(x1, x2, gs1, gs2, gs3, gs4);
                        Booking_factory.booking_register[Booking_factory.gen_reg_num].Booking_refP = x0;
                        Booking_factory.booking_register[Booking_factory.gen_reg_num].Driver_nameP = x3;
                        Booking_factory.booking_register[Booking_factory.gen_reg_num].Dietery_reqP = x4;
                        Booking_factory.booking_register[Booking_factory.gen_reg_num].Car_a_dateP = x5;
                        Booking_factory.booking_register[Booking_factory.gen_reg_num].Car_d_dateP = x6;
                        Booking_factory.booking_register[Booking_factory.gen_reg_num].CustomerP = x7;

                        if (b8 == "true") Booking_factory.booking_register[Booking_factory.gen_reg_num].BreakfastP = true;
                        else Booking_factory.booking_register[Booking_factory.gen_reg_num].BreakfastP = false;

                        if (b9 == "true") Booking_factory.booking_register[Booking_factory.gen_reg_num].DinnerP = true;
                        else Booking_factory.booking_register[Booking_factory.gen_reg_num].DinnerP = false;

                        if (b10 == "true") Booking_factory.booking_register[Booking_factory.gen_reg_num].CarP = true;
                        else Booking_factory.booking_register[Booking_factory.gen_reg_num].CarP = false;

                    }
                }
            }
        }

        private void Open_Cus()
        {
            String n = " ";
            String a = " ";

            using (StreamReader readtext = new StreamReader("write.txt"))
            {

                while ((a != null) || (n != null))
                {
                    n = readtext.ReadLine();
                    a = readtext.ReadLine();

                    if (n == "" || n == " " || n == null)
                        break;
                    else
                        Customer_factory.Create_customer(n, a);
                }
            }
        }
    }
}
