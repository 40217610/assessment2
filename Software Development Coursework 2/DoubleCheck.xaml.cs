﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for DoubleCheck.xaml
    /// </summary>
    public partial class DoubleCheck : Window
    {
        public DoubleCheck()
        {
            InitializeComponent();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            EditBooking.del_allowed = true;
            Close();
        }
    }
}
