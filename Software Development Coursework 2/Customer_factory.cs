﻿// Rafal Ozog 40217610
// Design pattern - Factory
// Class which is a Factory for customers. It is responsible for creating new customers, storing them in right order and providing
// access to them.
// 09.12.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Development_Coursework_2
{
    abstract class Customer_factory
    {
        // It's the dictionary, which contains all created customers with their appropriate indexes.
        public static Dictionary<int, Customer> customer_register = new Dictionary<int, Customer>();

        // "gen_reg_num" is a counter in the factory.
        public static int gen_reg_num = 0;

        // Method that creates new customer in the factory.
        public static void Create_customer(string n, string a)
        {
            gen_reg_num++;
            Customer c = new Customer(n, a, gen_reg_num);
            customer_register.Add(gen_reg_num, c);
        }

    }
}
