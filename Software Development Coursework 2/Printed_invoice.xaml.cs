﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for Printed_invoice.xaml
    /// </summary>
    public partial class Printed_invoice : Window
    {
        public static int booking_number;

        public Printed_invoice(int n)
        {
            InitializeComponent();

            booking_number = n;
            Booking b = Booking_factory.booking_register[n];

            txt1.Text = b.CustomerP;
            txt2.Text = n.ToString();
            txt3.Text = Bill_sub_system.Give_adults_amount(b).ToString();
            txt4.Text = Bill_sub_system.Give_children_amount(b).ToString();
            txt5.Text = Bill_sub_system.Give_nights_amount(b).ToString();
            txt6.Text = Bill_sub_system.Give_breakfasts_cost(b).ToString();
            txt7.Text = Bill_sub_system.Give_dinners_cost(b).ToString();
            txt8.Text = Bill_sub_system.Give_car_cost(b).ToString();
            txt9.Text = Bill_sub_system.Give_total_bill(b).ToString();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
          
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
