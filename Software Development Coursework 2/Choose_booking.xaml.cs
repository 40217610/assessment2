﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for Choose_booking.xaml
    /// </summary>
    public partial class Choose_booking : Window
    {
        public Dictionary<int, Booking>.ValueCollection values = Booking_factory.booking_register.Values;
        public Dictionary<int, Customer>.ValueCollection values2 = Customer_factory.customer_register.Values;
        public Dictionary<int, Customer>.KeyCollection keys2 = Customer_factory.customer_register.Keys;
        public List<int> customers_numbers_list = new List<int>();
        public List<String> booking_ref_list = new List<String>();

        public int choice = -1;
        public int chosen_customer = -1;

        public Choose_booking()
        {
            InitializeComponent();

            lst1.Items.Clear();
            int g; 

            // Uploading values into the showing list.
            foreach (Booking v in values)
            {
                g = 0;
                if (v.a != null) g++;
                if (v.b != null) g++;
                if (v.c != null) g++;
                if (v.d != null) g++;

                lst1.Items.Add("Booking number:     " + v.Booking_refP + ".     Customer:   " + v.CustomerP + ".      Dates:    " + v.A_dateP + " - " + v.D_dateP + ".     Guests amount:  " + g);
            }

            // Uploading values into the choosing combo box.
            foreach (Customer c in values2)
            {
                cbox1.Items.Add(c.NameP + " (ref. number: " + c.Reference_numP + " )");
                customers_numbers_list.Add(c.Reference_numP);
            }

            // Uploading reference numbers from the showed bookings into special list.
            booking_ref_list.Clear();
            foreach (Booking v in values)
            {
                booking_ref_list.Add(v.Booking_refP);
            }
        }

        private void bt3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lst1.SelectedIndex == -1)
                    throw new ArgumentException("You haven't chosen booking :) !");
                int booking_number = Int32.Parse(booking_ref_list[lst1.SelectedIndex]);

                EditBooking.r_num = booking_number;
                Close();
            }
            catch (Exception except) { MessageBox.Show("You haven't chosen booking :) !"); } 
        }

        private void bt4_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Window_Activated(object sender, EventArgs e)
        {
           
           
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (values.Count() == 0)
                    throw new ArgumentException("There is no data to show or filter :) !");

                choice = cbox1.SelectedIndex;
                chosen_customer = customers_numbers_list[choice];
                int g; // Guests amount.

                lst1.Items.Clear();

                foreach (Booking v in values)
                {
                    g = 0;
                    if (v.a != null) g++;
                    if (v.b != null) g++;
                    if (v.c != null) g++;
                    if (v.d != null) g++;

                    if (v.CustomerP == Customer_factory.customer_register[chosen_customer].NameP)
                        lst1.Items.Add("Booking number:     " + v.Booking_refP + ".     Customer:   " + v.CustomerP + ".      Dates:    " + v.A_dateP + " - " + v.D_dateP + ".     Guests amount:  " + g);
                }

                booking_ref_list.Clear();
                foreach (Booking v in values)
                {
                    if (v.CustomerP == Customer_factory.customer_register[chosen_customer].NameP)
                        booking_ref_list.Add(v.Booking_refP);
                }
            }
            catch (Exception except) { MessageBox.Show("There is no data to show or filter :) !"); } 
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (values.Count() == 0)
                    throw new ArgumentException("There is no data to show or filter :) !");

                lst1.Items.Clear();

                cbox1.SelectedIndex = -1;

                int g; // Guests amount.
                foreach (Booking v in values)
                {
                    g = 0;
                    if (v.a != null) g++;
                    if (v.b != null) g++;
                    if (v.c != null) g++;
                    if (v.d != null) g++;

                    lst1.Items.Add("Booking number:     " + v.Booking_refP + ".     Customer:   " + v.CustomerP + ".      Dates:    " + v.A_dateP + " - " + v.D_dateP + ".     Guests amount:  " + g);
                }

                booking_ref_list.Clear();
                foreach (Booking v in values)
                {
                    booking_ref_list.Add(v.Booking_refP);
                }
            }
            catch (Exception except) { MessageBox.Show("There is no data to show or filter :) !"); } 
        }

        private void lst1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
