﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for Edit_car.xaml
    /// </summary>
    public partial class Edit_car : Window
    {
        public static int b_num = EditBooking.r_num;

        public Edit_car()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            if (cb1.IsChecked == true)
                Booking_factory.booking_register[b_num].CarP = true;
            else
                Booking_factory.booking_register[b_num].CarP = false;

            Booking_factory.booking_register[b_num].Car_a_dateP = cal1.Text;
            Booking_factory.booking_register[b_num].Car_d_dateP = cal2.Text;

            Booking_factory.booking_register[b_num].Driver_nameP = txt1.Text;

            Close();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (Booking_factory.booking_register[b_num].CarP == true)
            {
                cb1.IsChecked = true;
                cal1.SelectedDate = DateTime.Parse(Booking_factory.booking_register[b_num].Car_a_dateP);
                cal2.SelectedDate = DateTime.Parse(Booking_factory.booking_register[b_num].Car_d_dateP);
                txt1.Text = Booking_factory.booking_register[b_num].Driver_nameP;
            }
        }
    }
}
