﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;  

namespace Software_Development_Coursework_2

{
    [TestClass]
    public class UnitTest
    {
        public Guest G;

         [TestMethod]
        // Creating guests.
        public void Creating_guests()
        {
            Guest G1 = new Guest("Janusz Tamson", 21, "2023123921AD");
            Assert.AreEqual("Janusz Tamson", G1.NameP, "Data assigned with error!");
            Assert.AreEqual(21, G1.AgeP, "Data assigned with error!");
            Assert.AreEqual("2023123921AD", G1.PassportP, "Data assigned with error!");
            G = G1;
        }

         [TestMethod]
         // Creating booking using factory.
         public void Creating_booking()
         {
             Booking_factory.Create_booking("01.01.2016", "05.01.2016", G, G, G, G);
             Assert.AreEqual(G, Booking_factory.booking_register[1].a, "Data assigned with error!");
         }

         [TestMethod]
         // Checking expected properties values in booking.
         public void Check_properties()
         {
             Assert.AreSame(1, Booking_factory.booking_register[1].Booking_refP, "Data assigned with error!");
             //Assert.AreSame("01-01-2016", Booking_factory.booking_register[1].A_dateP, "Data assigned with error!");
             //Assert.AreSame("05-01-2016", Booking_factory.booking_register[1].D_dateP, "Data assigned with error!");
         }

         [TestMethod]
         // Checking expected properties values in guest, existing inside the booking.
         public void Check_properties2()
         {
             Assert.AreSame("Janusz Tamson", Booking_factory.booking_register[1].a.NameP, "Data assigned with error!");
             //Assert.AreSame(21, Booking_factory.booking_register[1].a.AgeP, "Data assigned with error!");
             //Assert.AreSame("2023123921AD", Booking_factory.booking_register[1].a.PassportP, "Data assigned with error!");
         }

        
    }
}
