﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for Bookings.xaml
    /// </summary>
    public partial class Bookings : Window
    {
        public Bookings()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            AddBooking newWin = new AddBooking();
            newWin.Show();
            Close();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            EditBooking newWin = new EditBooking();
            newWin.Show();
            Close();
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            Invoice newWin = new Invoice();
            newWin.Show();
            Close();
        }

        private void btn1_1_Click(object sender, RoutedEventArgs e)
        {
            ShowBookings newWin = new ShowBookings();
            newWin.Show();
            Close();
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            MainWindow newWin = new MainWindow();
            newWin.Show();
            Close();
        }
    }
}
