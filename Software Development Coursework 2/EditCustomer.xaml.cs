﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for EditCustomer.xaml
    /// </summary>
    public partial class EditCustomer : Window
    {
        public Dictionary<int, Customer>.ValueCollection values;
        public Dictionary<int, Customer>.KeyCollection keys;
        public List<int> customers_numbers_list = new List<int>();
        public int chosen_element = -1;
        public int customer_id = -1;

        public EditCustomer()
        {
            InitializeComponent();
            Upload_customers();
        }

        public void Upload_customers()
        {
            customers_numbers_list.Clear();
            lbox1.Items.Clear();

            values = Customer_factory.customer_register.Values;
            keys = Customer_factory.customer_register.Keys;

            foreach (Customer c in values)
            {
                lbox1.Items.Add(c.NameP + "     (ref. number: " + c.Reference_numP + " )    Address: " + c.AddressP);
                customers_numbers_list.Add(c.Reference_numP);
            }
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            Customers newWin = new Customers();
            newWin.Show();
            Close();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lbox1.SelectedIndex == -1)
                    throw new ArgumentException("You haven't chosen booking :) !");
                chosen_element = lbox1.SelectedIndex;
                customer_id = customers_numbers_list[chosen_element];

                txt2.Text = Customer_factory.customer_register[customer_id].NameP;
                txt3.Text = Customer_factory.customer_register[customer_id].AddressP;
                txt4.Text = customer_id.ToString();

                btn2.IsEnabled = true;
                btn3.IsEnabled = true;
            }
            catch (Exception except) { MessageBox.Show("You haven't chosen customer :) !"); } 
        }

        private void lbox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            Customer_factory.customer_register[customer_id].NameP = txt2.Text;
            Customer_factory.customer_register[customer_id].AddressP = txt3.Text;

            btn2.IsEnabled = false;
            btn3.IsEnabled = false;

            txt2.Text = "";
            txt3.Text = "";
            txt4.Text = "";

            Upload_customers();
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            Customer_factory.customer_register.Remove(customer_id);
            txt2.Text = "";
            txt3.Text = "";
            txt4.Text = "";

            btn2.IsEnabled = false;
            btn3.IsEnabled = false;

            Upload_customers();
        }
    }
}
