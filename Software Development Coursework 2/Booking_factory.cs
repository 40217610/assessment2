﻿// Rafal Ozog 40217610
// Design pattern - Factory
// Class which is a Factory for bookings. It is responsible for creating new bookings, storing them in right order and providing
// access to them.
// 09.12.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Development_Coursework_2
{
    abstract class Booking_factory
    {
        // It's the dictionary, which contains all created bookings with their appropriate indexes.
        public static Dictionary<int, Booking> booking_register = new Dictionary<int, Booking>();

        // "gen_reg_num" is a counter in the factory.
        public static int gen_reg_num = 0;

        // Method that creates new booking in the factory.
        public static void Create_booking(String adate, String ddate, Guest g1, Guest g2, Guest g3, Guest g4)
        {
            gen_reg_num++;
            Booking b = new Booking(gen_reg_num.ToString(), adate, ddate, g1, g2, g3, g4);
            booking_register.Add(gen_reg_num, b);
        }

    }
}
