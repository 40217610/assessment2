﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for AddBooking2.xaml
    /// </summary>
    public partial class AddBooking2 : Window
    {
        public AddBooking2()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            if (cb1.IsChecked == true)
                Booking_factory.booking_register[Booking_factory.gen_reg_num].BreakfastP = true;

            if (cb2.IsChecked == true)
                Booking_factory.booking_register[Booking_factory.gen_reg_num].DinnerP = true;

            Booking_factory.booking_register[Booking_factory.gen_reg_num].Dietery_reqP = txt1.Text;

            if (cb3.IsChecked == true)
            {
                Booking_factory.booking_register[Booking_factory.gen_reg_num].CarP = true;
                Booking_factory.booking_register[Booking_factory.gen_reg_num].Car_a_dateP = cal1.Text;
                Booking_factory.booking_register[Booking_factory.gen_reg_num].Car_d_dateP = cal2.Text;
                Booking_factory.booking_register[Booking_factory.gen_reg_num].Driver_nameP = txt2.Text;
            }

            Bookings newWin = new Bookings();
            newWin.Show();
            Close();
        }

        private void cb3_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void cb3_Click(object sender, RoutedEventArgs e)
        {
            if (cb3.IsChecked == true)
            {
                lbl5.Visibility = System.Windows.Visibility.Visible;
                cal1.Visibility = System.Windows.Visibility.Visible;
                lbl6.Visibility = System.Windows.Visibility.Visible;
                cal2.Visibility = System.Windows.Visibility.Visible;
                lbl7.Visibility = System.Windows.Visibility.Visible;
                txt2.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                lbl5.Visibility = System.Windows.Visibility.Hidden;
                cal1.Visibility = System.Windows.Visibility.Hidden;
                lbl6.Visibility = System.Windows.Visibility.Hidden;
                cal2.Visibility = System.Windows.Visibility.Hidden;
                lbl7.Visibility = System.Windows.Visibility.Hidden;
                txt2.Visibility = System.Windows.Visibility.Hidden;
            }
        }
    }
}
