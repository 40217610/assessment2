﻿// Rafal Ozog 40217610
// Class which contains all booking objects details.
// 09.12.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Development_Coursework_2
{
    public class Booking
    {
        private String booking_ref;
        private String a_date;
        private String d_date;
        private String driver_name;
        private String dietery_req = "No special requirements";
        private String car_a_date;
        private String car_d_date;
        private String customer;
        private bool breakfast = false;
        private bool dinner = false;
        private bool car = false;
        private double bill;
        public Guest a, b, c, d;
        
        public String Booking_refP
        {
            get
            {
                return booking_ref;
            }
            set
            {
                booking_ref = value;
            }
        }

        public String A_dateP
        {
            get
            {
                return a_date;
            }
            set
            {
                if (value == "")
                    throw new ArgumentException("Empty arrival date!");
                a_date = value;
            }
        }

        public String D_dateP
        {
            get
            {
                return d_date;
            }
            set
            {
                if (value == "")
                    throw new ArgumentException("Empty departure date!");
                d_date = value;
            }
        }

        public bool BreakfastP
        {
            get
            {
                return breakfast;
            }
            set
            {
                breakfast = value;
            }
        }

        public bool DinnerP
        {
            get
            {
                return dinner;
            }
            set
            {
                dinner = value;
            }
        }

        public bool CarP
        {
            get
            {
                return car;
            }
            set
            {
                car = value;
            }
        }

        public String Driver_nameP
        {
            get
            {
                return driver_name;
            }
            set
            {
                driver_name = value;
            }
        }

        public String CustomerP
        {
            get
            {
                return customer;
            }
            set
            {
                customer = value;
            }
        }

        public String Dietery_reqP
        {
            get
            {
                return dietery_req;
            }
            set
            {
                dietery_req = value;
            }
        }

        public String Car_a_dateP
        {
            get
            {
                return car_a_date;
            }
            set
            {
                car_a_date = value;
            }
        }

        public String Car_d_dateP
        {
            get
            {
                return car_d_date;
            }
            set
            {
                car_d_date = value;
            }
        }

        public double BillP
        {
            get
            {
                return bill;
            }
            set
            {
                bill = value;
            }
        }

        public Booking(String r, String adate, String ddate, Guest g1, Guest g2, Guest g3, Guest g4)
        {
            booking_ref = r;
            
            if (adate == "")
                throw new ArgumentException("Empty departure date!");
            a_date = adate;

            if (ddate == "")
                throw new ArgumentException("Empty departure date!");
            d_date = ddate;

            if ((g1 == null) && (g2 == null) && (g3 == null) && (g4 == null))
                throw new ArgumentException("Empty departure date!");
            a = g1;
            b = g2;
            c = g3;
            d = g4;
        }

    }
}

