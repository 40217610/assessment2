﻿// Rafal Ozog 40217610
// Class which contains bill (invoice) details, provides calculations for Bill_sub_system, which only presents the results.
// 09.12.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Development_Coursework_2
{
    class Bill
    {
        private double total_bill = 0;
        private int adult_guests = 0;
        private int children_guests =0;
        private int nights = 0;
        private double car_hire = 0;
        private double breakfasts = 0;
        private double ev_meals = 0;

        // The constructor provides calculations and set appropriate values to local (this class properties) 
        // variables concerning the bill.
        public Bill(Booking x)
        {
            double nights = 0;

             if (x.a != null)
             {
                 if (x.a.AgeP <= 18) children_guests++;
                 else adult_guests++;    
             }

             if (x.b != null)
             {
                 if (x.b.AgeP <= 18) children_guests++;
                 else adult_guests++; 
             }

             if (x.c != null)
             {
                 if (x.c.AgeP <= 18) children_guests++;
                 else adult_guests++; 
             }

             if (x.d != null)
             {
                 if (x.d.AgeP <= 18) children_guests++;
                 else adult_guests++; 
             }

             DateTime ad = Convert.ToDateTime(x.A_dateP);
             DateTime dd = Convert.ToDateTime(x.D_dateP);
             TimeSpan dif = dd.Subtract(ad);
             nights = dif.TotalDays;
            
             total_bill = 50 * nights * adult_guests + 30 * nights * children_guests;

             if (x.BreakfastP == true)
             {
                 breakfasts = (nights * (adult_guests + children_guests) * 5);
                 total_bill += breakfasts;
             }

             if (x.DinnerP == true)
             {
                 ev_meals = (nights * (adult_guests + children_guests) * 15);
                 total_bill += ev_meals;
             }

            if (x.CarP == true)
            {
                DateTime dat1c = DateTime.Parse(x.Car_a_dateP);
                DateTime dat2c = DateTime.Parse(x.Car_d_dateP);

                TimeSpan car_rent = dat2c.Subtract(dat1c);
                double car_rent_days = car_rent.TotalDays;
                car_hire = 50 * car_rent_days;
                total_bill += car_hire;
            }
        }

        public double Total_billP
        {
            get
            {
                return total_bill;
            }
            set
            {   
                total_bill = value;
            }
        }

        public int Adult_guestsP
        {
            get
            {
                return adult_guests;
            }
            set
            {
                adult_guests = value;
            }
        }

        public int Children_guestsP
        {
            get
            {
                return children_guests;
            }
            set
            {
                children_guests = value;
            }
        }

        public int NightsP
        {
            get
            {
                return nights;
            }
            set
            {
                nights = value;
            }
        }

        public double Car_hireP
        {
            get
            {
                return car_hire;
            }
            set
            {
                car_hire = value;
            }
        }

        public double BreakfastsP
        {
            get
            {
                return breakfasts;
            }
            set
            {
                breakfasts = value;
            }
        }

        public double Ev_mealsP
        {
            get
            {
                return ev_meals;
            }
            set
            {
                ev_meals = value;
            }
        }
    }
}
