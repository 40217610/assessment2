﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for AddBooking.xaml
    /// </summary>
    public partial class AddBooking : Window
    {
        public Guest g1, g2, g3, g4;
        int chosen_key = -1;
        public List<int> lisia = new List<int>();
        public Dictionary<int, Customer>.ValueCollection values;
        public Dictionary<int, Customer>.KeyCollection keys;
        public bool customer_was_added = false;

        public AddBooking()
        {
            InitializeComponent();
        }


        // Btn1 adds new guest to local copy of variable and shows the details using ListBox.
        private void btn1_Click(object sender, RoutedEventArgs e)
        {

            if (g1 == null)
            {
                try
                {
                    g1 = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                    lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                    txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                }
                catch (Exception except) { MessageBox.Show("Wrong or incomplete guest data!"); }
            }
            else if (g2 == null)
                {
                    try
                    {
                        g2 = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                        lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                        txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                    }
                    catch (Exception except) { MessageBox.Show("Wrong or incomplete guest data!"); }
                }
            else if (g3 == null)
                {
                    try
                    {
                        g3 = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                        lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                        txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                    }
                    catch (Exception except) { MessageBox.Show("Wrong or incomplete guest data!"); }
                }
            else if (g4 == null)
                {
                    try
                    {
                        g4 = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                        lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                        txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                    }
                    catch (Exception except) { MessageBox.Show("Wrong or incomplete guest data!"); }
                    txt1.IsEnabled = false; txt2.IsEnabled = false; txt3.IsEnabled = false;
                    btn1.IsEnabled = false;
                }
        }

        // Btn2 creates our booking as an object and send to it first part of data (which we have just set).
        // Later the button is moving us into second stage of the booking creation process (another window).
        private void btn2_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Booking_factory.Create_booking(dat1.Text, dat2.Text, g1, g2, g3, g4);

                chosen_key = cbox1.SelectedIndex - 1;
                chosen_key = lisia[chosen_key];
                int c = Booking_factory.gen_reg_num;
                Customer_factory.customer_register[chosen_key].customer_bookings.Add(Booking_factory.booking_register[Booking_factory.gen_reg_num]);

                Booking_factory.booking_register[Booking_factory.gen_reg_num].CustomerP = Customer_factory.customer_register[chosen_key].NameP;

                txt1.Text = ""; txt2.Text = ""; txt3.Text = "";

                AddBooking2 newWin = new AddBooking2();
                newWin.Show();
                Close();
               
            }
            catch (Exception except) { MessageBox.Show("Date or guest missing!"); }
        }

        // Method that close the window without creating booking (button "Cancel").
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            g1 = null;
            g2 = null;
            g3 = null;
            g4 = null;

            Bookings newWin = new Bookings();
            newWin.Show();
            Close();
        }

        private void cbox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbox1.SelectedIndex == 0)
            {
                cbox1.Items.Clear();
                customer_was_added = true;
                AddCustomerMini newWin = new AddCustomerMini();
                newWin.Show();
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            values = Customer_factory.customer_register.Values;
            keys = Customer_factory.customer_register.Keys;
            foreach (int key in keys)
            {
                lisia.Add(key);
            }

            cbox1.Items.Clear();

            cbox1.Items.Add("Add new customer");
            foreach (Customer val in values)
            {
                cbox1.Items.Add(val.NameP + " (ref. number: " + val.Reference_numP + " )");
            }

            if (customer_was_added == true)
                cbox1.SelectedIndex = (cbox1.Items.Count);
        }

        private void cbox1_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void cbox1_TargetUpdated(object sender, DataTransferEventArgs e)
        {

        }

        private void cbox1_ManipulationCompleted(object sender, ManipulationCompletedEventArgs e)
        {

        }

        private void cbox1_Initialized(object sender, EventArgs e)
        {

        }

        private void cbox1_Drop(object sender, DragEventArgs e)
        {

        }

        private void cbox1_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void cbox1_PreviewDragEnter(object sender, DragEventArgs e)
        {

        }

        private void cbox1_PreviewDrop(object sender, DragEventArgs e)
        {

        }

        private void cbox1_TextInput(object sender, TextCompositionEventArgs e)
        {

        }

        
    }
}
