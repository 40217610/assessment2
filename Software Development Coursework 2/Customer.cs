﻿// Rafal Ozog 40217610
// Class which contains customer's details.
// 09.12.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Development_Coursework_2
{
    public class Customer
    {
        private String name;
        private String address;
        private int reference_num;
        public List<Booking> customer_bookings = new List<Booking>();

        public String NameP
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public String AddressP
        {
            get
            {
                return address;
            }
            set
            {
                address = value;
            }
        }

        public int Reference_numP
        {
            get
            {
                return reference_num;
            }
            set
            {
                reference_num = value;
            }
        }

        public Customer(String n, String a, int reference)
        {
            name = n;
            address = a;
            reference_num = reference;
        }

    }
}
