﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for AddCustomerMini.xaml
    /// </summary>
    public partial class AddCustomerMini : Window
    {
        public AddCustomerMini()
        {
            InitializeComponent();
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txt1.Text == "" || txt2.Text == "")
                    throw new ArgumentException("You haven't entered all required details :) !");
                Customer_factory.Create_customer(txt1.Text, txt2.Text);
                Close();
            }
            catch (Exception except) { MessageBox.Show("You haven't entered all required details :) !"); } 
        }
    }
}
