﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Development_Coursework_2
{
    /// <summary>
    /// Interaction logic for EditBooking.xaml
    /// </summary>
    public partial class EditBooking : Window
    {
        Guest a1, b1, c1, d1;
        public static int r_num = -1;
        public static bool del_allowed;

        public EditBooking()
        {
            InitializeComponent();
            r_num = -1;
            del_allowed = false;
        }

        public void btn0_Click(object sender, RoutedEventArgs e)
        {
            if (r_num.GetType() != typeof(int))
                throw new ArgumentException("Wrong value for 'Find'!");
            try 
            {
                r_num = Int32.Parse(txt0.Text);

                if (Booking_factory.booking_register.ContainsKey(r_num))
                {
                    a1 = Booking_factory.booking_register[r_num].a;
                    b1 = Booking_factory.booking_register[r_num].b;
                    c1 = Booking_factory.booking_register[r_num].c;
                    d1 = Booking_factory.booking_register[r_num].d;

                    txt0.Text = "";
                    dat1.SelectedDate = DateTime.Parse(Booking_factory.booking_register[r_num].A_dateP);
                    dat2.SelectedDate = DateTime.Parse(Booking_factory.booking_register[r_num].D_dateP);

                    if (Booking_factory.booking_register[r_num].a != null)
                    {
                        lbox1.Items.Add(Booking_factory.booking_register[r_num].a.NameP + " " + Booking_factory.booking_register[r_num].a.AgeP + " " + Booking_factory.booking_register[r_num].a.PassportP);
                    }
                    if (Booking_factory.booking_register[r_num].b != null)
                    {
                        lbox1.Items.Add(Booking_factory.booking_register[r_num].b.NameP + " " + Booking_factory.booking_register[r_num].b.AgeP + " " + Booking_factory.booking_register[r_num].b.PassportP);
                    }
                    if (Booking_factory.booking_register[r_num].c != null)
                    {
                        lbox1.Items.Add(Booking_factory.booking_register[r_num].c.NameP + " " + Booking_factory.booking_register[r_num].c.AgeP + " " + Booking_factory.booking_register[r_num].c.PassportP);
                    }
                    if (Booking_factory.booking_register[r_num].d != null)
                    {
                        lbox1.Items.Add(Booking_factory.booking_register[r_num].d.NameP + " " + Booking_factory.booking_register[r_num].c.AgeP + " " + Booking_factory.booking_register[r_num].d.PassportP);
                    }

                    if (lbox1.Items.Count == 4)
                    {
                        btn3.IsEnabled = false;
                        txt1.IsEnabled = false; txt2.IsEnabled = false; txt3.IsEnabled = false;
                    }
                  
                    btn1.IsEnabled = true;
                    btn3.IsEnabled = true;
                    btn4.IsEnabled = true;
                    btn6.IsEnabled = true;
                    btn7.IsEnabled = true;

                    lblx.Content = " Editing booking number: " + r_num.ToString();

                }
                else throw new ArgumentException("Nonexistent value for 'Find'!");
             }
            catch (Exception except) { MessageBox.Show("Wrong or nonexistent reservation number value!"); }
        }

        private void txt1_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        private void lbox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Booking_factory.booking_register[r_num].a == null)
                {
                    Booking_factory.booking_register[r_num].a = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                    lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                    txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                }
                else if (Booking_factory.booking_register[r_num].b == null)
                {
                    Booking_factory.booking_register[r_num].b = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                    lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                    txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                }
                else if (Booking_factory.booking_register[r_num].c == null)
                {
                    Booking_factory.booking_register[r_num].c = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                    lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                    txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                }
                else if (Booking_factory.booking_register[r_num].d == null)
                {
                    Booking_factory.booking_register[r_num].d = new Guest(txt1.Text, Int32.Parse(txt2.Text), txt3.Text);
                    lbox1.Items.Add(txt1.Text + " " + txt2.Text + " " + txt3.Text);
                    txt1.Text = ""; txt2.Text = ""; txt3.Text = "";
                }
            }
            catch (Exception except) { MessageBox.Show("Wrong/ incomplete guest data or the booking hasn't been chosen!"); }

            if (lbox1.Items.Count == 4)
            {
                btn3.IsEnabled = false;
                txt1.IsEnabled = false; txt2.IsEnabled = false; txt3.IsEnabled = false;
            }
        }

        private void ___btn4_Click(object sender, RoutedEventArgs e)
        {
            if (lbox1.SelectedIndex == 0)
            {
                Booking_factory.booking_register[r_num].a = Booking_factory.booking_register[r_num].b;
                Booking_factory.booking_register[r_num].b = Booking_factory.booking_register[r_num].c;
                Booking_factory.booking_register[r_num].c = Booking_factory.booking_register[r_num].d;
                Booking_factory.booking_register[r_num].d = null;
                lbox1.Items.RemoveAt(0);
            }
            if (lbox1.SelectedIndex == 1)
            {
                Booking_factory.booking_register[r_num].b = Booking_factory.booking_register[r_num].c;
                Booking_factory.booking_register[r_num].c = Booking_factory.booking_register[r_num].d;
                Booking_factory.booking_register[r_num].d = null;
                lbox1.Items.RemoveAt(1);
            }
            if (lbox1.SelectedIndex == 2)
            {
                Booking_factory.booking_register[r_num].c = Booking_factory.booking_register[r_num].d;
                Booking_factory.booking_register[r_num].d = null;
                lbox1.Items.RemoveAt(2);
            }
            if (lbox1.SelectedIndex == 3)
            {  
                Booking_factory.booking_register[r_num].d = null;
                lbox1.Items.RemoveAt(3);
            }

            if (lbox1.Items.Count < 4)
                    {
                        btn3.IsEnabled = true;
                        txt1.IsEnabled = true; txt2.IsEnabled = true; txt3.IsEnabled = true;
                    }
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            r_num = -1;

            Bookings newWin = new Bookings();
            newWin.Show();
            Close();
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Booking_factory.booking_register[r_num].A_dateP = dat1.Text;
                Booking_factory.booking_register[r_num].D_dateP = dat2.Text;
                r_num = -1;
                btn1.IsEnabled = false;
            }
            catch (Exception except) { MessageBox.Show("Missed reservation number, date or at least one guest!"); }
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            Edit_meal newWin = new Edit_meal();
            newWin.Show();
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            Edit_car newWin = new Edit_car();
            newWin.Show();
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            if (r_num != -1)
            {
                try
                {
                    if (Booking_factory.booking_register.ContainsKey(r_num))
                    {
                        a1 = Booking_factory.booking_register[r_num].a;
                        b1 = Booking_factory.booking_register[r_num].b;
                        c1 = Booking_factory.booking_register[r_num].c;
                        d1 = Booking_factory.booking_register[r_num].d;

                        // Cleaning of the ListBox.
                        lbox1.Items.Clear();


                        txt0.Text = "";
                        dat1.SelectedDate = DateTime.Parse(Booking_factory.booking_register[r_num].A_dateP);
                        dat2.SelectedDate = DateTime.Parse(Booking_factory.booking_register[r_num].D_dateP);

                        if (Booking_factory.booking_register[r_num].a != null)
                        {
                            lbox1.Items.Add(Booking_factory.booking_register[r_num].a.NameP + " " + Booking_factory.booking_register[r_num].a.AgeP + " " + Booking_factory.booking_register[r_num].a.PassportP);
                        }
                        if (Booking_factory.booking_register[r_num].b != null)
                        {
                            lbox1.Items.Add(Booking_factory.booking_register[r_num].b.NameP + " " + Booking_factory.booking_register[r_num].b.AgeP + " " + Booking_factory.booking_register[r_num].b.PassportP);
                        }
                        if (Booking_factory.booking_register[r_num].c != null)
                        {
                            lbox1.Items.Add(Booking_factory.booking_register[r_num].c.NameP + " " + Booking_factory.booking_register[r_num].c.AgeP + " " + Booking_factory.booking_register[r_num].c.PassportP);
                        }
                        if (Booking_factory.booking_register[r_num].d != null)
                        {
                            lbox1.Items.Add(Booking_factory.booking_register[r_num].d.NameP + " " + Booking_factory.booking_register[r_num].c.AgeP + " " + Booking_factory.booking_register[r_num].d.PassportP);
                        }

                        if (lbox1.Items.Count == 4)
                        {
                            btn3.IsEnabled = false;
                            txt1.IsEnabled = false; txt2.IsEnabled = false; txt3.IsEnabled = false;
                        }

                        btn1.IsEnabled = true;
                        btn3.IsEnabled = true;
                        btn4.IsEnabled = true;
                        btn6.IsEnabled = true;
                        btn7.IsEnabled = true;
                        btn12.IsEnabled = true;

                        lblx.Content = (" Booking number: " + r_num.ToString());
                    }
                    else throw new ArgumentException("Nonexistent value for 'Find'!");
                }
                catch (Exception except) { MessageBox.Show("Wrong or nonexistent reservation number value!"); }
            }

            if (del_allowed == true)
            {
                lbox1.Items.Clear();
                Booking_factory.booking_register.Remove(r_num);
                del_allowed = false;
                r_num = -1;
            }
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            Choose_booking newWin = new Choose_booking();
            newWin.Show();
        }

        private void btn12_Click(object sender, RoutedEventArgs e)
        {
            lbox1.Items.Clear();
            DoubleCheck newWin = new DoubleCheck();
            newWin.Show();
        }
    }
}
