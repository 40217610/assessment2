﻿// Rafal Ozog 40217610
// Class which contains guest object details.
// 09.12.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Development_Coursework_2
{
    public class Guest
    {
        private String name;
        private int age;
        private String passport;

        public String NameP
        {
            get
            {
                return name;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("Wrong guest data!");
                name = value;
            }
        }

        public int AgeP
        {
            get
            {
                return age;
            }
            set
            {
                if (value.GetType() != typeof(int))
                    throw new ArgumentException("Wrong guest data!");
                age = value;
            }
        }

        public String PassportP
        {
            get
            {
                return passport;
            }
            set
            {
                if (value == "")
                    throw new ArgumentException("Wrong guest data!");
                passport = value;
            }
        }
        
        public Guest (String n, int a, String p)
        {
            name = n;
            age = a;
            passport = p;
        }
    }
}
