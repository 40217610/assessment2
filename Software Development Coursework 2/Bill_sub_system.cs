﻿// Rafal Ozog 40217610
// Design pattern - Facade
// Class which is a Facade, which provides only methods to operate among bill (invoice) details, hiding the implementation
// in another class (Bill).
// 09.12.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Development_Coursework_2
{
    class Bill_sub_system
    {
        public static double Give_total_bill(Booking ex)
        {
            Bill b = new Bill(ex);
            return b.Total_billP;
        }

        public static int Give_adults_amount(Booking ex)
        {
            Bill b = new Bill(ex);
            return b.Adult_guestsP;
        }

        public static int Give_children_amount(Booking ex)
        {
            Bill b = new Bill(ex);
            return b.Children_guestsP;
        }

        public static int Give_nights_amount(Booking ex)
        {
            Bill b = new Bill(ex);
            return b.NightsP;
        }

        public static double Give_car_cost(Booking ex)
        {
            Bill b = new Bill(ex);
            return b.Car_hireP;
        }

        public static double Give_breakfasts_cost(Booking ex)
        {
            Bill b = new Bill(ex);
            return b.BreakfastsP;
        }

        public static double Give_dinners_cost(Booking ex)
        {
            Bill b = new Bill(ex);
            return b.Ev_mealsP;
        }
    }
}
